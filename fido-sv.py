import socket
import threading
from _thread import *
import time

RET_PIN = 'pin certified'
RET_NFC = 'nfc certified'

# 1. 시나리오
# from NFC/TAG정보/Device정보
# to nfc certified
# from PIN/PIN정보/Device정보
# to pin certified
def parse_data(recv_data):

    data = recv_data.decode().split('/')

    if len(data) != 3:
        print('FORMAT ERROR')
        return 'format error(should be \'NFC/AAA/BBB\'), you sent \"{0}\"'.format(recv_data.decode())

    if data[0] == 'NFC':
        ret = RET_NFC
    elif data[0] == 'PIN':
        ret = RET_PIN
    else:
        ret = 'neither NFC nor PIN, you sent \"{0}\"'.format(recv_data.decode())

    ret = ret + "\r\n"
    print('PARSE: ', ret)
    return ret

def threaded(c):
    #while True:
    recv_data = c.recv(1024)
    #    if not recv_data:
    #        break
    print ("받은 데이터: ", recv_data.decode())
    send_data = parse_data(recv_data).encode() 
    print ("보내는 데이터: ", send_data)
    c.sendall(send_data)
    print ("메시지 전송 완료")
    c.sendall(send_data)
    print ("메시지 전송 완료")
    c.sendall(send_data)
    print ("메시지 전송 완료")

    c.close()
    print("커넥션 종료")

def Main(): 
    print('소켓 서버 초기화')
    host = ''
    port = 4000

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host,port))
    s.listen(10)
    while True:
        c, addr = s.accept()
        print(addr[0], ':', addr[1], '연결')
        start_new_thread(threaded,(c,))

if __name__ == '__main__': 
    Main() 
