import socket

SERVER = 'localhost'
PORT = 4000

def run():
    while True:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((SERVER, PORT))
            line = input(':')
            s.sendall(line.encode())
            resp = s.recv(1024)
            print(f'>{resp.decode()}')

if __name__ == '__main__':
    run()
