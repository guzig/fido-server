import socket 
from time import sleep
  
from _thread import *
import threading 
  
print_lock = threading.Lock() 
  
def threaded(c): 
    while True: 
  
        c.send(b'welcome!')

        data = c.recv(1024) 
        print(str(data))
        if not data: 
            print('Bye') 
              
            print_lock.release() 
            break
  
        print(str(data)) 
  
        c.send(data) 
  
    c.close() 
  
  
def Main(): 

    host = "" 
  
    port = 4000
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s: 
        s.bind((host, port)) 
        print("socket binded to post", port) 

        while True:  
                s.listen(1) 
                print("socket is listening") 

  
                c, addr = s.accept()
                print('Connected to :', addr[0], ':', addr[1]) 

                while True:
                        data = c.recv(1024)
                        print(data.decode())
                        if not data: break
                        c.send(data)
                c.close()
    
  
if __name__ == '__main__': 
    Main() 
